﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using SomaNumeros;

namespace API.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class SomaNumerosController : ControllerBase
    {
        private readonly ILogger<SomaNumerosController> _logger;

        public SomaNumerosController(ILogger<SomaNumerosController> logger) => _logger = logger;

        [HttpGet]
        public int Somar(int n1, int n2) => n1.Somar(n2);
    }
}
